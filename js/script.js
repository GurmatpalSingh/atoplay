(function($) {
  
  "use strict";



    //  ==================== SCROLLING FUNCTION ====================
    if ($(window).width() >= 768) {
    $(window).on("scroll", function() {
        var scroll = $(window).scrollTop();
        if (scroll > 30) {
            $(".top_bar").addClass("scroll animated slideInDown");
        } else if (scroll < 30) {
            $(".top_bar").removeClass("scroll animated slideInDown")
        }
    });
}

    var header_height = $(".top_bar").innerHeight() + 2;
  
    $(".side_menu-innr").css({
        "top": header_height
		
    });
    if ($(window).width() >= 768) {
    var header_height = $(".top_bar").innerHeight() + 2;
    $(".side_menu").css({
        "top": header_height
		
    });
}

    $(".menu").on("click", function(){
      $(".side_menu").toggleClass("active");
      return false;
    });

    $("html").on("click", function() {
        $(".side_menu").removeClass("active");
    });
    $(".menu, .side_menu").on("click", function(e){
        e.stopPropagation();
    });

    $(".menu").on("click", function(){
        $(".side_menu-innr").toggleClass("active");
        return false;
      });
  
      $("html").on("click", function() {
          $(".side_menu-innr").removeClass("active");
      });
      $(".menu, .side_menu-innr").on("click", function(e){
          e.stopPropagation();
      });
  

    $(".user-log").on("click", function() {
        $(".account-menu").slideToggle();
    });
    $("html").on("click", function() {
        $(".account-menu").slideUp();
    });
    $(".user-log, .account-menu").on("click", function(e) {
        e.stopPropagation();
    });


    //  ==================== SCROLLING FUNCTION ====================
    
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })

    $(document).ready(function () {
        var owl = $('.gc-owl-carousel');
        owl.owlCarousel({
          slideBy: 1,
          autoWidth: true,
          margin: 10,
          nav: true,
          loop: false,
          dots: false,
          paginationSpeed: 1000,
          responsiveClass: true,
          navContainerClass: 'gc-nav',
          navText: [
            "<img src='images/left-chevron.png' />",
            "<img src='images/right-chevron.png' />"
          ],
          responsive: {
            0: {
              items: 1
            },
            600: {
              items: 3
            },
            1000: {
              items: 5
            }
          }
        })
      })
    
      $(document).ready(function () {
        $('[data-toggle=search-form]').click(function () {
          $('.search-form-wrapper').toggleClass('open');
          $('.search-form-wrapper .search').focus();
          $('html').toggleClass('search-form-open');
        });
        $('[data-toggle=search-form-close]').click(function () {
          $('.search-form-wrapper').removeClass('open');
          $('html').removeClass('search-form-open');
        });
        $('.search-form-wrapper .search').keypress(function (event) {
          if ($(this).val() == "Search") $(this).val("");
        });
        $('.search-close').click(function (event) {
          $('.search-form-wrapper').removeClass('open');
          $('html').removeClass('search-form-open');
        });
        if ($(window).width() <= 992) {
          $('.side_menu').removeClass('side-sw');
          $('.close-menu').addClass('d-none');
          $('.side_menu').addClass('mb-menu');
          $('.menu-btn').removeClass('menu-btn-open');
        }
      });


})(window.jQuery);


function openNav() {
    $('.side_menu').addClass('side-sw');
    $('.left-sect').addClass('left-sw');
    $('.close-menu').removeClass('d-none');
    $('.close-menu a').css('display', 'block');
    $('.gc-owl-carousel').owlCarousel();
    $('.menu-btn').addClass('menu-btn-open');
  }
  function closeNav() {
    $('.side_menu').removeClass('side-sw');
    $('.left-sect').removeClass('left-sw');
    $('.close-menu').addClass('d-none');
    $('.close-menu a').css('display', 'none');
    $('.gc-owl-carousel').owlCarousel();
    $('.menu-btn').removeClass('menu-btn-open');
  }